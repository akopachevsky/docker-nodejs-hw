FROM node:6.3

RUN mkdir -p /user/src/app1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN git clone https://bitbucket.org/akopachevsky/docker-nodejs-hw.git /usr/src/app

EXPOSE 8080

CMD ["npm", "start"]