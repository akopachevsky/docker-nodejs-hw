#!/bin/bash

CLUSTER_NAME="my-cluster"
SERVICE_NAME="node-hw-service"
TASK_FAMILY="node-hw-task"

# stop service by stopping all tasks
aws ecs update-service --cluster ${CLUSTER_NAME} --service ${SERVICE_NAME} --desired-count 0

sleep 120

# deregister old task
OLD_TASK_REVISION=`aws ecs describe-task-definition --task-definition $TASK_FAMILY| egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//'`
if [ -z ${OLD_TASK_REVISION+x} ]; then  
	echo 'no old task registered';
else 
	aws ecs deregister-task-definition --task-definition ${TASK_FAMILY}:${OLD_TASK_REVISION}; 
fi

# create new revision for task definition 
sed -e "s;%BUILD_NUMBER%;${BUILD_NUMBER};g" node-hw-task-definition.json > /tmp/node-hw-task-definition.json
aws ecs register-task-definition --cli-input-json file:///tmp/node-hw-task-definition.json
TASK_REVISION=`aws ecs describe-task-definition --task-definition $TASK_FAMILY| egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//'`

# update service with new task definition
aws ecs update-service --cluster ${CLUSTER_NAME} --service ${SERVICE_NAME} --task-definition ${TASK_FAMILY}:${TASK_REVISION} --desired-count 1


