

apt-get update 
apt-get install -y curl
apt-get install -y libgmp-dev build-essential openssl libreadline6 libreadline6-dev zlib1g zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt-dev autoconf libc6-dev gawk ncurses-dev automake libtool bison ssl-cert pkg-config libgdbm-dev libffi-dev clang llvm llvm-dev libedit-dev subversion


useradd  --create-home --disabled-password web

su - web -c 'command curl -sSL https://rvm.io/mpapis.asc | gpg --import -'
su - web -c 'curl -sSL https://get.rvm.io | bash -s stable'

su - web -c 'rvm install 2.2.2 --default'

su - web -c 'rvm use 2.2.2@project --create --default'

su - web -c 'ruby -v'

su - web -c 'gem install sass'

su - web -c 'rvm gemset list'


passenger 


sudo apt-get install -y curl gnupg build-essential


# Install our PGP key and add HTTPS support for APT
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
apt-get install -y --force-yes apt-transport-https ca-certificates

# Add our APT repository
sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main > /etc/apt/sources.list.d/passenger.list'
apt-get update

# Install Passenger + Nginx
sudo apt-get install -y nginx-extras passenger


# app 

sudo apt-get install -y nodejs &&
sudo ln -sf /usr/bin/nodejs /usr/local/bin/node


sudo apt-get install -y git

sudo mkdir -p /var/www/myapp
sudo chown web: /var/www/myapp

sudo -u web -H git clone https://akopachevsky@bitbucket.org/akopachevsky/rails-hello-world-postgres.git code


sed -e "s;%BUILD_NUMBER%;$BUILD_NUMBER;g" node-hw-task-definition.json > /tmp/node-hw-task-definition.json
aws ecs register-task-definition --cli-input-json file:///tmp/node-hw-task-definition.json