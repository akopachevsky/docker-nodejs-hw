var http = require('http');

var server = http.createServer(function(req, res) {
  res.writeHead(200);
  res.end('Hello Node-Docker 11');
});
const PORT = 8080;
server.listen(PORT);
console.log('Running on http://localhost:' + PORT);